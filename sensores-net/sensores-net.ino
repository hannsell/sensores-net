#include <SPI.h>
#include <LoRa.h>
#include <Cipher.h>
#include "ABlocks_DHT.h"
#include <ArduinoJson.h>

double tipoMensajeNuevo;
double esmaestro;
double temp;
int idNodo;
int idDestino;
int idSolicitado;
double hum;
double umbralTemperatura;
double CO2;
double GPSLA;
double numeroNodos;
double GPSLO;
int id_origen;
double tipoMensaje;

bool puedo_pedir_datos_a_esclavos = false;

bool nodo_cercano_establecido = false;

String s_tipoMensajeConsultaNodoCercano;
String s_tipoMensajeNodoCercanoResponse;
String s_tipoMensajeRecibeControlMaestro;
String s_tipoMensajeAnulaMaestro;
String s_tipoMensajeMaestro;
String s_fecha;
String s_hora;
String s_tipoMensajeReceptDataMaster;
String s_tipoMensajeEsclavo;
String s_tipoMensajeSatelite;
String s_datos_json;
String s_tipoMensaje;
boolean b_puedeVerificarUmbral;
boolean b_soyMaestro;
boolean b_umbralTemperaturaSuperado;
String lora_data_received="";
int lora_data_rssi=0;
Cipher *lora_data_cipher = new Cipher();
bool lora_data_cipher_active = false;
String lora_data_cipher_pw = "";
String ls_tipoMensajes[3]={s_tipoMensajeMaestro, s_tipoMensajeEsclavo, String("")};
int ls_tipoMensajes_size=3;
unsigned long task_time_ms=0;

unsigned long task_time_ms2=0;

DHT dht26(26,DHT11);
StaticJsonDocument<1500> json_data;
char json_temp_data[1500];
DeserializationError json_error;

bool getPuedeVerificarUmbral() {
  return b_puedeVerificarUmbral;
}
void setPuedeVerificarUmbral() {
	b_puedeVerificarUmbral = true;
}
void setNoPuedeVerificarUmbral() {
	b_puedeVerificarUmbral = false;
}
void setSoyMaestro() {
  setPuedeVerificarUmbral();
  nodo_cercano_establecido=false;
	b_soyMaestro = true;
}
void setTipoMensaje(double tipoMensajeNuevo) {
	tipoMensajeNuevo = tipoMensajeNuevo;
}
void setNoEsMaestro() {
  setNoPuedeVerificarUmbral();
	b_soyMaestro = false;
}
void fnc_lora_send(String _data){
	LoRa.beginPacket();
	if(lora_data_cipher_active){
		LoRa.print(lora_data_cipher->encryptString(_data));
	}
	else{
		LoRa.print(_data);
	}
	LoRa.endPacket();
}

void fnc_lora_onreceive(int _packetSize){

  String s_tipoMensajeEntrante = "";
	lora_data_rssi=0;
	if (_packetSize == 0) return;

	String lora_data_received_temp="";
	lora_data_received="";

	while (LoRa.available()) {
		lora_data_received_temp += (char)LoRa.read();
	}

	if(lora_data_cipher_active){
		lora_data_received=lora_data_cipher->decryptString(lora_data_received_temp);
	}
	else{
		lora_data_received=lora_data_received_temp;
	}

	lora_data_rssi=LoRa.packetRssi();

 	json_error = deserializeJson(json_data,(lora_data_received).c_str());

  Serial.println(String("post json_error: ")+String(_packetSize));

	if ((!json_error)) {
                Serial.println(String(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"));
                Serial.println(String("fnc_lora_onreceive -> entra ok mensaje JSON "));

                Serial.println(String("Recibir data LORA"));
                s_tipoMensajeEntrante = fnc_json_get_key_text(String("tipoMensaje"));
                Serial.println(String("Mensaje recibido de tipo: "));
                Serial.println(s_tipoMensajeEntrante);
              
                /**
                  Mensajes de un maestro
                  entonces el nodo es esclavo
                */

                if(!esMaestro()){


                  if (String(s_tipoMensajeMaestro).equals(String(s_tipoMensajeEntrante))) {
                    /**
                      Esta entrando a un nodo que es esclavo.
                    */
                    Serial.println(String("Esclavo Recibiendo Mensaje de maestro"));
                    // setNoEsMaestro();
                    idSolicitado = fnc_json_get_key_number(String("idDestino"));
                    Serial.println(String("Enviando mensaje a maestro: ")+String(idSolicitado));

                    actuarEsclavo();
                  }
                  
                  if (String(s_tipoMensajeConsultaNodoCercano).equals(String(s_tipoMensajeEntrante))) {
                      /**
                        Esta entrando a un nodo que es esclavo.
                      */
                      if ((dht26.readTemperature() < umbralTemperatura)) {
                        Serial.println(String("Esclavo Recibiendo Mensaje consulta nodo cercano"));
                        id_origen = fnc_json_get_key_number(String("idOrigen"));
                        Serial.println(id_origen);
                        nodoCercanoResonse(); //el nodo esclavo responde al maestro
                      }
                  }

                  //mensaje que convierte en maestro a un esclavo
                  if (String(s_tipoMensajeRecibeControlMaestro).equals(String(s_tipoMensajeEntrante))) {
                    /**
                      Esta entrando a un nodo que es esclavo.
                    */
                    Serial.println(String("Esclavo Recibiendo Mensaje de maestro antiguo"));
                    
                    guardarDataDelMaestroAntiguo(fnc_json_get_key_number(String("idOrigen")));
                  }

                 
                }
                /**
                  Mensajes de un esclavo 
                  Entonces el nodo es un maestro
                */


                if(esMaestro()){     

                  //el maestro que esta entregando el control recibe un mensaje de que se anule como maestro para que no pida mas datos.
                  if (String(s_tipoMensajeAnulaMaestro).equals(String(s_tipoMensajeEntrante))) {
                    /**
                      Esta entrando a un nodo que es maestro.
                    */
                    Serial.println(String("Recibiendo Mensaje de anular maestro"));
                    setNoEsMaestro();
                  }                  

                  if (String(s_tipoMensajeEsclavo).equals(String(s_tipoMensajeEntrante)) ) {
                    Serial.println(String("Recibiendo Mensaje de un esclavo"));
                    Serial.println(String("Hacer las cosas con los datos del esclavo que reporta"));
                    id_origen = fnc_json_get_key_number(String("idOrigen"));
                    temp = fnc_json_get_key_number(String("T"));
                    hum = fnc_json_get_key_number(String("H"));
                    CO2 = fnc_json_get_key_number(String("C"));
                    Serial.print(String("Datos recibidos del nodo: "));
                    Serial.println(id_origen);
                    Serial.print(String("Tipo de Mensaje:"));
                    Serial.println(s_tipoMensajeEntrante);
                    Serial.print(String("Temperatura:  "));
                    Serial.println(temp);
                    Serial.print(String("Humedad: "));
                    Serial.println(hum);
                    Serial.print(String("CO2: "));
                    Serial.println(CO2);
                    Serial.println(String("----------------************-------------"));
                  }
                

                  if (String(s_tipoMensajeNodoCercanoResponse).equals(String(s_tipoMensajeEntrante))) {
                    /**
                      Escucha la respuesta de las respuestas de los nodos.
                    */
                    if(!nodo_cercano_establecido){

                      // si no se puede verificar mas el umbral es porque ya tiene respuesta de un nodo cercano y sera el nuevo maestro(el nodo que resondio).
                      Serial.println(String("1.- bloqueo recepcion de nuevas respuestas de nodos"));

                      //no puedo verificar el umbral para evitar que siga recibiendo mensajes de otros nodos cercanos.
                      setNoPuedeVerificarUmbral();
                      nodo_cercano_establecido = true;
                      id_origen = fnc_json_get_key_number(String("idOrigen"));
                      Serial.println(String("Este sera el nuevo maestro"));
                      Serial.println(id_origen);
                      Serial.println(String("2.- Transferir datos al nuevo maestro"));
                      enviarDatosAlmacenadosAlNuevoMaestro(fnc_json_get_key_number(String("idOrigen")));
                    }
                  }

                }
     
                /**
                  Mensajes de un satelite 
                */
	}else{
     Serial.println(String("Malformed data JSON"));
  }
}//fin lora envent


void enviarDatosAlmacenadosAlNuevoMaestro(int idOrigenNuevoMaestro ){
  Serial.println(String("Funcion para iniciar la transferencia de datos al nuevo maestro"));
  String json=String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("idDestino")+String("\"")+String(":")+String(idOrigenNuevoMaestro)))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeRecibeControlMaestro)+String("\"")))+String("}");
  Serial.println(String(json));

 	fnc_lora_send(json);
}


void guardarDataDelMaestroAntiguo(int idOrigenAntiguoMaestro){

    Serial.println(String("Se debe guardar data del maestro antiguo"));
    //ahora este nodo se convierte en maestro
    setSoyMaestro();
    Serial.println(String("mandar mensaje al maestro antiguo para que sea un esclavo. al idDestino : "));
    Serial.println(String(idOrigenAntiguoMaestro));
    //se le dice al antiguo maestro que ya no es maestro.
    //fnc_lora_send(String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeConsultaNodoCercano)+String("\"")))+String("}"));
    String s_datos_json_a_antiguo_maestro=String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("idDestino")+String("\"")+String(":")+String(idOrigenAntiguoMaestro)))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeAnulaMaestro)+String("\"")))+String("}");
    Serial.println(String(s_datos_json_a_antiguo_maestro));
    fnc_lora_send(s_datos_json_a_antiguo_maestro);

}
 

bool esMaestro() {
  return (b_soyMaestro == true);
}
bool verificarUmbralTemperatura() {
	b_umbralTemperaturaSuperado = false;
	if ((dht26.readTemperature() > umbralTemperatura)) {
		b_umbralTemperaturaSuperado = true;
	}

  return b_umbralTemperaturaSuperado;
}
void preguntaPorNodoCercano() {
	Serial.println(String("Pregunta por el nodo mas cercano"));
	fnc_lora_send(String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeConsultaNodoCercano)+String("\"")))+String("}"));
}
void nodoCercanoResonse() {
	Serial.println(String("Responde al maestro que es el nodo cercano"));
	fnc_lora_send(String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeNodoCercanoResponse)+String("\"")))+String("}"));
}
void actuarMaestro() {
  Serial.println(String("Siguiente nodo"));
	Serial.print(String("Solicitud al nodo: "));
	Serial.println(idDestino);
	fnc_lora_send(String("{")+String((String("\"")+String("idDestino")+String("\"")+String(":")+String(idDestino)))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeMaestro)+String("\"")))+String("}"));
	idDestino=idDestino+(1);
	if ((idDestino > numeroNodos)) {
		idDestino = 1;
    Serial.println(String("Reinicia conteo de nodos"));
		Serial.println(String("---------------------------------------"));
	}

}
String fnc_json_get_key_text(String _f){
	if(json_error)return "";
  JsonVariant t=json_data[_f];
  if( t.is<JsonObject>() || t.is<JsonArray>() || t.is<JsonObjectConst>() || t.is<JsonArrayConst>() ){
      serializeJson(t, json_temp_data);
      return String(json_temp_data);
  }
  return String(t.as<const char*>());
}

double fnc_json_get_key_number(String _f){
	if(json_error)return sqrt(-1);
  JsonVariant t=json_data[_f];
  if( (!t.is<double>()) && (!t.is<int>()) ){
      return 0;
  }
  return (double)(t.as<double>());
}

boolean fnc_json_get_key_bool(String _f){
	if(json_error)return false;
  JsonVariant t=json_data[_f];
  if(!t.is<boolean>() ){
      return 0;
  }
  return (boolean)(t.as<boolean>());
}

double fnc_json_get_array_size(String _f){
	if(json_error)return 0;
  if(_f=="")return json_data.size();
  JsonArray t=json_data[_f];
  if(t){
      return t.size();
  }
  return 0;
}

String  fnc_json_get_index_text(int _i){
	if(json_error)return "";
  JsonVariant t=json_data[_i];
  if( t.is<JsonObject>() || t.is<JsonArray>() || t.is<JsonObjectConst>() || t.is<JsonArrayConst>() ){
      serializeJson(t, json_temp_data);
      return String(json_temp_data);
  }
  return String(t.as<const char*>());
}

double fnc_json_get_index_number(int _i){
	if(json_error)return sqrt(-1);
  JsonVariant t=json_data[_i];
  if( (!t.is<double>()) && (!t.is<int>()) ){
      return sqrt(-1);
  }
  return (double)(t.as<double>());
}

boolean fnc_json_get_index_bool(int _i){
	if(json_error)return false;
  JsonVariant t=json_data[_i];
  if( !t.is<boolean>() ){
      return false;
  }
  return (boolean)(t.as<boolean>());
}

void actuarEsclavo() {
  Serial.println(String("Actuando como esclavo para enviar los datos: ")+String("idSolicitado : ")+String(idSolicitado)+String("idNodo: ")+String(getIdNodo()));
  Serial.println(String("############################################################################################################"));
  String data_json_actuar_esclavo = "";
	if ((idSolicitado == getIdNodo())) {
		if (((!isnan(temp)) && (!isnan(hum)))) {
      data_json_actuar_esclavo = getDataPerifericosJSON();
      Serial.print(String("Enviando datos via fnc_lora_send"));
      fnc_lora_send(data_json_actuar_esclavo);
      Serial.print(String("Datos enviados via fnc_lora_send"));
			Serial.println(data_json_actuar_esclavo);
			Serial.print(String("Datos enviados a maestro via JSON"));
		}
	}

}




String getDataPerifericosJSON(){
    double temp = dht26.readTemperature();
		double hum = dht26.readHumidity();
		double CO2 = 42;
		GPSLA = (long)(123689);
		GPSLO = (long)(789101);
		s_fecha = String("29112023");
		s_hora = String("20h48m");
		if (((!isnan(temp)) && (!isnan(hum)))) {
			s_datos_json = String("{")+String((String("\"")+String("idOrigen")+String("\"")+String(":")+String(getIdNodo())))+String(",")+String((String("\"")+String("tipoMensaje")+String("\"")+String(":")+String("\"")+String(s_tipoMensajeEsclavo)+String("\"")))+String(",")+String((String("\"")+String("H")+String("\"")+String(":")+String(hum)))+String(",")+String((String("\"")+String("C")+String("\"")+String(":")+String(CO2)))+String(",")+String((String("\"")+String("GLA")+String("\"")+String(":")+String(GPSLA)))+String(",")+String((String("\"")+String("GLO")+String("\"")+String(":")+String(GPSLO)))+String(",")+String((String("\"")+String("F")+String("\"")+String(":")+String("\"")+String(s_fecha)+String("\"")))+String(",")+String((String("\"")+String("h")+String("\"")+String(":")+String("\"")+String(s_hora)+String("\"")))+String(",")+String((String("\"")+String("T")+String("\"")+String(":")+String(temp)))+String("}");
      Serial.print(String("Enviando datos via fnc_lora_send"));
      fnc_lora_send(s_datos_json);
      Serial.print(String("Datos enviados via fnc_lora_send"));
			Serial.println(s_datos_json);
			Serial.print(String("Datos enviados a maestro via JSON"));
		}
  return s_datos_json;
}

void guardarDatos(String dataJson){
  	Serial.print(String("Aqui se deben guardar los datos en la SD"));
}



void setIdNodo(int i_idNodo) {
  idNodo = i_idNodo;
}

int getIdNodo(){
  return idNodo;
}

void setup()
{
  	pinMode(26, INPUT);

	Serial.begin(115200);
	Serial.flush();
	while(Serial.available()>0)Serial.read();

	dht26.begin();

	if(lora_data_cipher_active){
		lora_data_cipher->setKey("");
	}
	LoRa.setPins(5,13,12);
	LoRa.begin(868E6);
 
  setIdNodo(1);
  puedo_pedir_datos_a_esclavos = true; //cuando es maestro puede pedir datos 

	umbralTemperatura = 30;
	idDestino = 1;
	numeroNodos = 3;
	b_soyMaestro = true;
	b_puedeVerificarUmbral = true;
  
  Serial.println(String("imprime los valores de la variable verificar umbral"));
	Serial.println(b_puedeVerificarUmbral);
	Serial.println((getPuedeVerificarUmbral()));

	s_tipoMensajeMaestro = String("maestro");
	s_tipoMensajeEsclavo = String("esclavo");
	s_tipoMensajeSatelite = String("satelite");
	s_tipoMensajeConsultaNodoCercano = String("consulta_nodo_cercano");
	s_tipoMensajeNodoCercanoResponse = String("nodo_cercano_response");
	s_tipoMensajeReceptDataMaster = String("recept_data_master");
  s_tipoMensajeRecibeControlMaestro = String("recibe_control_maestro");
  s_tipoMensajeAnulaMaestro = String("anula_maestro");
	s_tipoMensaje = s_tipoMensajeMaestro;

	Serial.println(String(">>>>>>>>>>>FIN INICIALIZAR<<<<<<<<<<"));

}


void loop()
{

	fnc_lora_onreceive(LoRa.parsePacket());
  	if((millis()-task_time_ms)>=15000){
  		task_time_ms=millis();
  		Serial.println(String("Ciclo"));
  		Serial.println(String("Soy el Nodo: ")+String(getIdNodo()));
  		Serial.println(String("Mi temperatura es: ")+String( dht26.readTemperature() ));
  		Serial.println(String("esMaestro: ")+String(esMaestro()));
  		Serial.println(String("getPuedeVerificarUmbral: ")+String(esMaestro()));

  		if (esMaestro() && getPuedeVerificarUmbral()) {
  			actuarMaestro();
  		}
  		else {
  			Serial.println(String("Debo mandar un mensaje tipo esclavo cuando un maestro pida informacion."));
  		}
  	}
  	if((millis()-task_time_ms2)>=3000){
  		
      task_time_ms2=millis();
      //si es un nodo maestro y la temperatura supera el umbral y puede verificar el umbral entonces puede solicitar nodo cercano
      if ((esMaestro() && verificarUmbralTemperatura() && getPuedeVerificarUmbral())) {
        Serial.print(String("Varible puede revisar umbral: "));
        Serial.println((getPuedeVerificarUmbral()));
        Serial.print(String("verificado el umbral pasa el limite de temperatura y busca un nodo cercano"));
  			preguntaPorNodoCercano();
  		}

  	}

	yield();
}